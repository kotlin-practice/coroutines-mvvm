package com.practice.coroutiusapicall

data class DataModelItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)