package com.practice.coroutiusapicall

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface APiInterface {

    @GET("posts")
    fun getData(): Call<List<DataModelItem>>
}