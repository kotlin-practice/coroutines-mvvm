package com.practice.coroutiusapicall

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import retrofit2.await

class DataViewModel : ViewModel() {

    val dataCall : LiveData<List<DataModelItem?>> = MutableLiveData()

    init {
        viewModelScope.launch {
            dataCall as MutableLiveData
            dataCall.value = getDataCall()
        }
    }

    private suspend fun getDataCall():List<DataModelItem?>{
        val response = ApiUtils.getRetrofit().create(APiInterface::class.java).getData()

        return withContext(Dispatchers.IO){
            response.await()
        }

    }

}