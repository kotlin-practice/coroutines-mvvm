package com.practice.coroutiusapicall

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView

    private var list: List<DataModelItem?> = ArrayList()
    private lateinit var dataViewModel: DataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getInit()

        dataViewModel.dataCall.observe(this , Observer {
            val gson=Gson()
            var string = gson.toJson(it)
            d("dataChecking",string)
            list=it
            recyclerView.adapter = DataAdapter(this, list)

        })

    }

    private fun getInit() {
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        dataViewModel = ViewModelProvider(this).get(DataViewModel::class.java)
    }
}