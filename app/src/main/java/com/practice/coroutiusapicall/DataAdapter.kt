package com.practice.coroutiusapicall

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.practice.coroutiusapicall.DataAdapter.*

class DataAdapter(private val context: Context, private val list: List<DataModelItem?>) :
    RecyclerView.Adapter<DataAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.model_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.idTv.setText(""+list.get(position)?.id)
        holder.useridTv.setText(""+list.get(position)?.userId)
        holder.titleTv.setText(list.get(position)?.title)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var idTv :TextView = itemView.findViewById(R.id.idTv)
        var useridTv :TextView = itemView.findViewById(R.id.useridTv)
        var titleTv :TextView = itemView.findViewById(R.id.titleTv)

    }
}